using UnityEngine;

public class millardevans : MonoBehaviour
{
    public AudioClip clickSound, cameraSound;

    public static millardevans USE;

    private AudioSource tabithasheehan;

    private void Awake()
    {
       
        if (USE == null)
        {
            USE = this;
            DontDestroyOnLoad(gameObject);

            tabithasheehan = transform.GetChild(0).GetComponent<AudioSource>();

            augustamilligan();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void augustamilligan()
    {
        
        AudioListener.volume = PlayerPrefs.GetInt("MusicSetting", 1);
    }

    public void winifredmarrero()
    {
        AudioListener.volume = AudioListener.volume == 1 ? 0 : 1;

        PlayerPrefs.SetInt("MusicSetting", (int)AudioListener.volume);
        PlayerPrefs.Save();
    }

    public void dorothealynch(AudioClip clip)
    {
        tabithasheehan.PlayOneShot(clip);
    }
}
