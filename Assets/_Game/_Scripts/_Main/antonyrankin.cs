using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class antonyrankin : MonoBehaviour
{
    public Camera cameraObj;
    public tybaxter coloringMenu, paintingMenu;

    [System.Serializable]
    public class tybaxter
    {
        public GameObject darlenebonilla;
        public Color color;
        public Image image;
        public Sprite leeferguson;
        public Sprite nicholeortiz;
    }

    void Awake()
    {
        Camera.main.aspect = 16 / 9f;
    }

    void Start()
    {
        OnMenuButtonClicked(false);
    }

    public void OnMenuButtonClicked(bool isPainting)
    {
        PlayerPrefs.SetInt("isPainting", isPainting ? 1 : 0);
        PlayerPrefs.Save();

        paintingMenu.darlenebonilla.SetActive(isPainting);
        coloringMenu.darlenebonilla.SetActive(!isPainting);

        cameraObj.backgroundColor = isPainting ? paintingMenu.color : coloringMenu.color;
        paintingMenu.image.sprite = isPainting ? paintingMenu.leeferguson : paintingMenu.nicholeortiz;
        coloringMenu.image.sprite = !isPainting ? coloringMenu.leeferguson : coloringMenu.nicholeortiz;
    }

    public void velmagilliam()
    {

    }

    public void lorrainebradford()
    {
        if (rufuswoodruff.Instance.eileenhinkle)
        {
            SceneManager.LoadScene("gms");

        }
    }
}
