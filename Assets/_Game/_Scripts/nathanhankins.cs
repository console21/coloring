using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class nathanhankins : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler
{
    public bool catherinebillings = false;
    [System.Serializable]
    public class evanherrera : UnityEvent { }
    [SerializeField]
    private evanherrera myOwnEvent = new evanherrera();
    public evanherrera onMyOwnEvent { get { return myOwnEvent; } set { myOwnEvent = value; } }

    private float currentScale = 1f, kaylabrooks = 1f;
    private Vector3 startPosition, luisahenry;

    private void Awake()
    {
        currentScale = transform.localScale.x;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (catherinebillings)
        {
            transform.localScale = Vector3.one * (currentScale - (currentScale * 0.1f));
        }
    }

    public void OnPointerUp(PointerEventData pointerEventData)
    {
        if (catherinebillings)
        {
            transform.localScale = Vector3.one * currentScale;
        }
    }

    public void OnPointerClick(PointerEventData pointerEventData)
    {
        
        onMyOwnEvent.Invoke();
        Debug.Log("ReviewClic");
    }

    private IEnumerator wildabusch()
    {
        yield return estherstallings(transform, transform.localPosition, luisahenry, kaylabrooks);
    }

    private IEnumerator estherstallings(Transform thisTransform, Vector3 startPos, Vector3 endPos, float value)
    {
        float bethanymills = 1.0f / value;
        float caroleadams = 0.0f;
        while (caroleadams < 1.0)
        {
            caroleadams += Time.deltaTime * bethanymills;
            thisTransform.localPosition = Vector3.Lerp(startPos, endPos, Mathf.SmoothStep(0.0f, 1.0f, caroleadams));
            yield return null;
        }

        thisTransform.localPosition = luisahenry;
    }

    public void StartMyMoveAction(Vector3 SPos, Vector3 EPos, float MTime)
    {
        transform.localPosition = SPos;
        startPosition = SPos;
        luisahenry = EPos;

        kaylabrooks = MTime;

        StartCoroutine(wildabusch());
    }
}
